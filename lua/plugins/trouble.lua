return {
 "folke/trouble.nvim",
 dependencies = { "nvim-tree/nvim-web-devicons" },
 opts = {
    position = "bottom",
    icons = true,
    padding = true,
  },
  config = function()
    vim.keymap.set("n", "<leader>t", "<cmd>Trouble<CR>", {})
    vim.keymap.set("n", "<leader>tc", "<cmd>TroubleClose<CR>", {})
    vim.keymap.set("n", "<leader>tr", "<cmd>TroubleRefresh<CR>", {})
  end
}
